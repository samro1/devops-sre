# Devops and SRE

# Cultura DevOps

# Conceptos y Practicas de DevOps
    - Construccion automatizada.
    - Integracion continua.
    - Entrega Continua y Despliegue continuo.
    - Infraestructura como codigo.
    - Gestion de configuraciones.
    - Orquestacion.
    - Monitoreo.
        - Recoleccion de datos de monitoreo.
            - Uso de memoria.
            - cpu.
            - disk i/o
            - Otros recursos en tiempo real.
            - logs de aplicaciones.
            - trafico de red.
            - Etc
        - Recolectar toda la informacion de diferentes formatos y presentar graficas de funcionamiento.
        - Notificaciones en tiempo real sobre problemas.
            - Cuando el rendimiento del sitio web ha comenzado a relentizarse.
            - Una herramienta detecta que el tiempo de respuesta esta crecciendo.
            - El administrador es immediatamente notificado y asi interviene antes que una caida ocurra.
    - Microservicios.



# Notes
NOW, orchestator


SRE
SLO/SLI. 
https://sre.google/sre-book/service-level-objectives/


Uptime monitoring

Pingdom
Apica

Exceptions trackers
Sentry.io

Definite: Structure logins, 
node (pino)
java 


Metricas.

DataDogs Historgrams Pages.
Es importante mencionar que cuando tienes logs y herramientas de monitoreo hay distintas formas de monitorear un aplicativo ya que algunos pueden ser:
-Browser (alguna ruta critica por ejemplo un carrito de compra)
-Infraestructura como instancias o servidores
-Application Performance Monitoring (APM)
etc

Por lo tanto es muy importante saber como SRE que monitorear para resolver que situación por ejemplo si es un aplicativo web es imporante ver estos factores:
-Largest Contentful Paint (LCP)
-First Input Delay (FID)
-Cumulative Layout Shift (CLS)
De esa forma resolver si es un tema del browser, del CDN, del aplicativo o de la Infraestrcutra

https://web.dev/i18n/es/vitals/

No midamos cosas triviales, midamos cosas que impacten nuestro servicio. Lo mismo para los logs, ambas cosas cuestan, sólo reportemos lo importante.



