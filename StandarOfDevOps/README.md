# Buenas practicas Kubernetes
# 1. Establecer límites y solicitudes de recursos.
Cuando está implementando una gran aplicación en un clúster de producción con recursos limitados donde los nodos se quedan sin memoria o CPU, la aplicación dejará de funcionar. Este tiempo de inactividad de la aplicación puede tener un gran impacto en el negocio. Pero puede resolver esto teniendo solicitudes y límites de recursos.

Las solicitudes y los límites de recursos son los mecanismos de Kubernetes para controlar el uso de recursos como la memoria y la CPU. Si un pod consume toda la CPU y la memoria, los otros pods se quedarán sin recursos y no podrán ejecutar la aplicación. Por lo tanto, debe establecer solicitudes de recursos y límites en los pods para aumentar la confiabilidad.

Solo para su información, el límite siempre será mayor que la solicitud. Su contenedor no se ejecutará si su solicitud supera el límite definido. Puede establecer solicitudes y límites para cada contenedor en un pod. La CPU se define usando milicores y la memoria usando bytes (megabyte / mebibyte).

A continuación se muestra un ejemplo de cómo establecer un límite de CPU de 500 milicores y 128 mebibytes, y establecer una cuota para solicitudes de CPU de 300 milicores y 64 mebibytes.

```
containers:
- name: prodcontainer1
    image: ubuntu
    resources:
        requests:
            memory: “64Mi”
            cpu: “300m”
        limits:                              
            memory: “128Mi”
            cpu: “500m”
```

# 2. Utilice livenessProbe y readinessProbe
Las comprobaciones de estado son muy importantes en Kubernetes.

Proporciona dos tipos de controles de salud: Preparación sondas y Vivacidad sondas.

Las sondas de preparación se utilizan para verificar si la aplicación está lista para comenzar a brindar tráfico o no. Esta sonda debe pasar Kubernetes antes de comenzar a enviar el tráfico al pod que ejecuta la aplicación dentro de un contenedor. Kubernetes dejará de enviar tráfico al pod hasta que falle esta verificación de estado de preparación.

Las sondas de Liveness se utilizan para verificar si la aplicación aún se está ejecutando (viva) o si se ha detenido (muerta). Si la aplicación se ejecuta correctamente, Kubernetes no hace nada. Si su aplicación está muerta, Kubernetes lanzará un nuevo pod y ejecutará la aplicación en él.

Si estas comprobaciones no se realizan correctamente, los pods pueden terminar o comenzarán a recibir las solicitudes de los usuarios incluso antes de que estén listos.

Hay tres tipos de sondas que se pueden utilizar para las comprobaciones de actividad y preparación: HTTP, Mando, y TCP.

Permítanme mostrarles un ejemplo del más común que es la sonda HTTP.

Aquí su aplicación tendrá un servidor HTTP dentro. Cuando Kubernetes hace ping en una ruta al servidor HTTP y obtiene una respuesta HTTP, marcará que la aplicación está en buen estado, de lo contrario no está en buen estado.
```
apiVersion: v1
kind: Pod
metadata:
 name: container10
spec:
 containers:
   - image: ubuntu
     name: container10
     livenessProbe:
       httpGet:
         path: /prodhealth
         port: 8080
```

# 3. Crear imágenes de contenedores pequeños
Es preferible utilizar imágenes de contenedores más pequeños porque requiere menos almacenamiento y podrá extraer y crear las imágenes más rápido. Dado que el tamaño de la imagen será menor, las posibilidades de ataques de seguridad también serán menores.

Hay dos formas de reducir el tamaño del contenedor: utilizando una imagen base más pequeña y un patrón de construcción. Actualmente, la última imagen base de NodeJS es de 345 MB, mientras que la imagen alpina de NodeJS es de solo 28 MB, más de diez veces más pequeña. Por lo tanto, siempre use las imágenes más pequeñas y agregue las dependencias necesarias para ejecutar su aplicación.

Para mantener las imágenes del contenedor aún más pequeñas, puede utilizar un patrón de construcción. El código se construye en el primer contenedor y luego el código compilado se empaqueta en el contenedor final sin todos los compiladores y herramientas necesarios para hacer el código compilado, haciendo que la imagen del contenedor sea aún más pequeña.

# 4. Otorgar niveles seguros de acceso (RBAC)
Tener un clúster de Kubernetes seguro es muy importante.

El acceso al clúster debe estar bien configurado. Debe definir el número de solicitudes por usuario por segundo / minuto / hora, las sesiones simultáneas permitidas por dirección IP, el tamaño de la solicitud y el límite de rutas y nombres de host. Esto ayudará a mantener el clúster a salvo de Los ataques DDoS.

Los desarrolladores y los ingenieros de DevOps que trabajan en un clúster de Kubernetes deben tener un nivel de acceso definido. La función de control de acceso basado en roles (RBAC) de Kubernetes es útil aquí. Puede utilizar Roles y ClusterRoles para definir los perfiles de acceso. Para facilitar la configuración de RBAC, puede utilizar código abierto gerentes-rbac disponible para ayudarlo a simplificar la sintaxis o el uso Rancher, proporciona RBAC de forma predeterminada.

```
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: cluster-role
rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["get", "list"]
```

Los secretos de Kubernetes almacenan información confidencial, como tokens de autenticación, contraseñas y claves ssh. Nunca debe verificar los secretos de Kubernetes en el IAC repositorio, de lo contrario, estará expuesto a aquellos que tengan acceso a su repositorio de git.

DevSecOps es una palabra de moda ahora que habla de DevOps y seguridad. Las organizaciones están adoptando la tendencia porque comprenden su importancia.

# 5. Mantenerse al día
Se recomienda tener siempre la última versión de Kubernetes instalada en el clúster.

La última versión de Kubernetes incluye nuevas funciones, actualizaciones de funciones anteriores, actualizaciones de seguridad, correcciones de errores, etc. Si está utilizando Kubernetes con un proveedor en la nube, actualizarlo es muy fácil.

# 6. Usar espacios de nombres
Kubernetes incluye tres espacios de nombres diferentes: tu préstamo estudiantil, sistema kube, y kube-público.

Estos espacios de nombres juegan un papel muy importante en un clúster de Kubernetes para la organización y la seguridad entre los equipos.

Tiene sentido usar el espacio de nombres predeterminado si es un equipo pequeño que trabaja solo entre 5 y 10 microservicios. Pero un equipo en rápido crecimiento o una gran organización tendrá varios equipos trabajando en un entorno de prueba o producción, por lo que cada equipo debe tener un espacio de nombres separado para facilitar la administración.

Si no lo hacen, pueden terminar sobrescribiendo accidentalmente o interrumpiendo la aplicación / función de otro equipo sin siquiera darse cuenta. Se sugiere crear varios espacios de nombres y usarlos para segmentar sus servicios en fragmentos manejables.

A continuación, se muestra un ejemplo de creación de recursos dentro de un espacio de nombres:
```
apiVersion: v1
kind: Pod
metadata:
   name: pod01
namespace: prod
   labels:
      image: pod01
spec:
   containers:
- name: prod01
  Image: debian
```
# 7. Usar etiquetas
A medida que crecen sus implementaciones de Kubernetes, invariablemente incluirán múltiples servicios, pods y otros recursos. Hacer un seguimiento de estos puede resultar engorroso. Aún más desafiante puede ser describir Kubernetes cómo interactúan estos diversos recursos, cómo desea que se repliquen, escalen y mantengan. Las etiquetas en Kubernetes son muy útiles para resolver estos problemas.

Las etiquetas son pares clave-valor que se utilizan para organizar elementos dentro de la interfaz de Kubernetes.

Por ejemplo, app: kube-app, fase: prueba, rol: front-end. Se utilizan para describir Kubernetes cómo funcionan juntos varios objetos y recursos dentro del clúster.

```
apiVersion: v1
kind: Pod
metadata:
 name: test-pod
 labels:
   environment: testing
   team: test01
spec:
 containers:
   - name: test01
     image: "Ubuntu"
     resources:
       limits:
        cpu: 1
```
Por lo tanto, puede reducir el dolor de la producción de Kubernetes etiquetando siempre los recursos y los objetos.

# 8. Registro de auditoría
Para identificar amenazas en el clúster de Kubernetes, la auditoría de registros es muy importante. La auditoría ayuda a responder preguntas como qué sucedió, por qué sucedió, quién hizo que sucediera, etc.

Todos los datos relacionados con las solicitudes realizadas a kube-apiserver se almacenan en un archivo de registro llamado audit.log. Este archivo de registro está estructurado en formato JSON.

En Kubernetes, de forma predeterminada, el registro de auditoría se almacena en <em>/var/log/audit.log</em> y la política de auditoría está presente en <em>/etc/kubernetes/audit-policy.yaml</em>.

Para habilitar el registro de auditoría, inicie kube-apiserver con estos parámetros:

--audit-policy-file=/etc/kubernetes/audit-policy.yaml --audit-log-path=/var/log/audit.log
Aquí hay una muestra audit.log archivo configurado para registrar cambios en los pods:

```
apiVersion: audit.k8s.io/v1
kind: Policy
omitStages:
  - "RequestReceived"
rules:
  - level: RequestResponse
    resources:
    - group: ""
      resources: ["pods"]
   - level: Metadata
     resources:
     - group: ""
      resources: ["pods/log", "pods/status"]
```

Siempre puede volver atrás y verificar los registros de auditoría en caso de cualquier problema en el clúster de Kubernetes. Le ayudará a restaurar el estado correcto del clúster.

# 9. Aplicar reglas de afinidad (nodo / pod)
Hay dos mecanismos en Kubernetes para asociar pods con los nodos de una mejor manera: Vaina y Nodo afinidad. Se recomienda utilizar estos mecanismos para un mejor rendimiento.

Con la afinidad de nodos, puede programar pods en los nodos según criterios definidos. Según los requisitos de pod, el nodo coincidente se selecciona y se asigna en un clúster de Kubernetes.

```
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu
spec:
  affinity:
    nodeAffinity:    
preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 2
        preference:        
matchExpressions:
          - key: disktype
            operator: In
            values:
            - ssd          
  containers:
  - name: ubuntu
    image: ubuntu
    imagePullPolicy: IfNotPresent

```
Con la afinidad de pods, puede programar varios pods en el mismo nodo (para mejorar la latencia) o decidir mantener los pods en nodos separados (para alta disponibilidad) para aumentar el rendimiento.

```
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-pod
spec:
  affinity:
    podAffinity:
     
requiredDuringSchedulingIgnoredDuringExecution:
      - labelSelector:
         
matchExpressions:
          - key: security
            operator: In
            values:
            - S1
        topologyKey: failure-domain.beta.kubernetes.io/zone
  containers:
  - name: ubuntu-pod
    image: debian
```

Después de analizar la carga de trabajo de su clúster, debe decidir qué estrategia de afinidad utilizar.

# 10. Terminación de Kubernetes
Kubernetes finaliza los pods cuando ya no son necesarios. Puede iniciarlo mediante un comando o una llamada a la API, los pods seleccionados pasan al estado de terminación y no se envía tráfico a esos pods. A continuación, se envía un mensaje SIGTERM a esos pods, después de lo cual se apagan.

Los pods se terminan correctamente; de ​​forma predeterminada, el período de gracia es de 30 segundos. Si los pods aún se están ejecutando, Kubernetes envía un mensaje SIGKILL que apaga los pods a la fuerza. Finalmente, Kubernetes elimina estos pods del servidor API en la máquina maestra.

En caso de que sus cápsulas siempre tarden más de 30 segundos, puede aumentar este período de gracia a 45 o 60 segundos.
```
apiVersion: v1
kind: Pod
metadata:
 name: container10
spec:
 containers:
   - image: ubuntu
     name: container10
  terminationGracePeriodSeconds: 60
```

# Terraform
# Structuring
Cuando esté trabajando en un gran proyecto de infraestructura de producción utilizando Terraform, debe seguir una estructura de directorio adecuada para ocuparse de las complejidades que puedan ocurrir en el proyecto. Sería mejor si tuviera directorios separados para diferentes propósitos.

Por ejemplo, si está utilizando terraform en entornos de desarrollo, preparación y producción, tenga directorios separados para cada uno de ellos.

geekflare@geekflare:~$ tree terraform_project/
terraform_project/
├── dev
│ ├── main.tf
│ ├── outputs.tf
│ └── variables.tf
├── modules
│ ├── ec2
│ │ ├── ec2.tf
│ │ └── main.tf
│ └── vpc
│ ├── main.tf
│ └── vpc.tf
├── prod
│ ├── main.tf
│ ├── outputs.tf
│ └── variables.tf
└── stg
├── main.tf
├── outputs.tf
└── variables.tf

6 directories, 13 files
Incluso las configuraciones de terraform deben estar separadas porque, después de un período, las configuraciones de una infraestructura en crecimiento se volverán complejas.

Por ejemplo, puede escribir todos sus códigos terraform (módulos, recursos, variables, salidas) dentro del main.tf archivo en sí, pero tener códigos terraform separados para variables y salidas lo hace más legible y fácil de entender.

# Naming Convention
Las convenciones de nomenclatura se utilizan en Terraform para hacer las cosas fácilmente comprensibles.

Por ejemplo, supongamos que desea crear tres espacios de trabajo diferentes para diferentes entornos en un proyecto. Entonces, en lugar de nombrarlos como env1, en2, env3, debe llamarlos como dev, escenario, pinchar. A partir del nombre en sí, queda bastante claro que hay tres espacios de trabajo diferentes para cada entorno.

También deben seguirse convenciones similares para recursos, variables, módulos, etc. El nombre del recurso en Terraform debe comenzar con un nombre de proveedor seguido de un guión bajo y otros detalles.

Por ejemplo, el nombre del recurso para crear un objeto terraform para una tabla de rutas en AWS sería aws_route_table.

Por lo tanto, si sigue correctamente las convenciones de nomenclatura, será más fácil comprender incluso los códigos complejos.

# Use Shared Modules
Se recomienda encarecidamente utilizar los módulos oficiales de Terraform disponibles. No es necesario reinventar un módulo que ya existe. Ahorra mucho tiempo y dolor. Registro terraform tiene muchos módulos disponibles. Realice cambios en los módulos existentes según la necesidad.

Además, cada módulo debe concentrarse en un solo aspecto de la infraestructura, como crear una instancia AWS EC2, configurar la base de datos MySQL, etc.

Por ejemplo, si desea usar AWS VPC en su código terraform, puede usar: VPC simple

module "vpc_example_simple-vpc" {
source
= "terraform-aws-modules/vpc/aws//examples/simple-vpc"
version = "2.48.0"
}

# Latest Version
La comunidad de desarrollo de Terraform es muy activa y el lanzamiento de nuevas funcionalidades ocurre con frecuencia. Se recomienda permanecer en la última versión de Terraform como cuando ocurre una nueva versión importante. Puede actualizar fácilmente a la última versión.

Si omite varias versiones importantes, la actualización se volverá muy compleja.

Corral terraform -v comando para comprobar si hay una nueva actualización.

geekflare@geekflare:~$ terraform -v
Terraform v0.11.14
Your version of Terraform is out of date! The latest version
is 0.12.0. You can update by downloading from www.terraform.io/downloads.html
Backup System State
Siempre haga una copia de seguridad de los archivos de estado de Terraform.

Estos archivos realizan un seguimiento de los metadatos y los recursos de la infraestructura. Por defecto, estos archivos llamados como terraform.tfstate se almacenan localmente dentro del directorio del espacio de trabajo.

Sin estos archivos, Terraform no podrá determinar qué recursos se implementan en la infraestructura. Por lo tanto, es esencial tener una copia de seguridad del archivo de estado. De forma predeterminada, un archivo con un nombre terraform.tfstate.backup se creará para mantener una copia de seguridad del archivo de estado.

geekflare@geekflare:~$ tree terraform_demo/
terraform_demo/
├── awsec2.tf
├── terraform.tfstate
└── terraform.tfstate.backup
0 directories, 3 files
Si desea almacenar un archivo de estado de respaldo en alguna otra ubicación, use -backup flag en el comando terraform y proporcione la ruta de ubicación.

La mayoría de las veces, habrá varios desarrolladores trabajando en un proyecto. Por lo tanto, para darles acceso al archivo de estado, debe almacenarse en una ubicación remota utilizando un terraform_remote_state fuente de datos.

El siguiente ejemplo tomará una copia de seguridad en S3.

data "terraform_remote_state" "vpc" {
backend = "s3"
config = {
bucket = “s3-terraform-bucket”
key = “vpc/terraform.tfstate"
region = “us-east-1”
   }
}

# Lock State File
Puede haber varios escenarios en los que más de un desarrollador intente ejecutar la configuración de terraform al mismo tiempo. Esto puede provocar la corrupción del archivo de estado de terraform o incluso la pérdida de datos. El mecanismo de bloqueo ayuda a prevenir tales escenarios. Se asegura de que a la vez, solo una persona esté ejecutando las configuraciones de terraform y no haya ningún conflicto.

A continuación, se muestra un ejemplo de bloqueo del archivo de estado, que se encuentra en una ubicación remota mediante DynamoDB.

resource “aws_dynamodb_table” “terraform_state_lock” {
name = “terraform-locking”
read_capacity = 3
write_capacity = 3
hash_key = “LockingID”

attribute {
name = “LockingID”
type = “S”
   }

}
terraform {
backend “s3” {
bucket = “s3-terraform-bucket”
key = “vpc/terraform.tfstate”
region = “us-east-2”
dynamodb_table = “terraform-locking”
   }
}
Cuando varios usuarios intentan acceder al archivo de estado, el nombre de la base de datos de DynamoDB y la clave principal se utilizarán para bloquear el estado y mantener la coherencia.

Nota:: no todos los backend admiten el bloqueo.

# Use self Variable
self variable es un tipo especial de variable que se usa cuando no se conoce el valor de la variable antes de implementar una infraestructura.

Supongamos que desea usar la dirección IP de una instancia que se implementará solo después del comando terraform apply, por lo que no conoce la dirección IP hasta que esté en funcionamiento.

En tales casos, usa auto variables, y la sintaxis para usarlo es self.ATTRIBUTE. Entonces, en este caso, usará self.ipv4_address como una variable propia para obtener la dirección IP de la instancia. Estas variables solo se permiten en bloques de conexión y aprovisionamiento de la configuración de terraform.

connection {
host = self.ipv4_address
type = "ssh"
user = var.users[2]
private_key = file(var.private_key_path)
}
Minimize Blast Radius
El radio de explosión no es más que la medida del daño que puede ocurrir si las cosas no salen según lo planeado.

Por ejemplo, si está implementando algunas configuraciones de terraform en la infraestructura y la configuración no se aplica correctamente, cuál será la cantidad de daño a la infraestructura.

Por lo tanto, para minimizar el radio de explosión, siempre se sugiere impulsar algunas configuraciones en la infraestructura a la vez. Por lo tanto, si algo salió mal, el daño a la infraestructura será mínimo y podrá corregirse rápidamente. Implementar muchas configuraciones a la vez es muy arriesgado.

Use var-file
En terraform, puede crear un archivo con extensión <em>.</em>tfvars y pase este archivo al comando terraform apply usando -var-file bandera. Esto le ayuda a pasar aquellas variables que no quiere poner en el código de configuración de terraform.

Siempre se sugiere pasar variables para una contraseña, clave secreta, etc. localmente a través de -var-archivo en lugar de guardarlo dentro de las configuraciones de terraform o en un sistema de control de versiones de ubicación remota.

Por ejemplo, si desea lanzar una instancia ec2 usando terraform, puede pasar la clave de acceso y la clave secreta usando -var-archivo

Crear un archivo terraform.tfvars y ponga las claves en este archivo.

geekflare@geekflare:~$ gedit terraform.tfvars

access_key = "AKIATYWSDFYU5DUDJI5F"
secret_key = "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"
Ahora, use este archivo var en el comando terraform.

geekflare@geekflare:~$ terraform apply -var-file=/home/geekflare/terraform.tfvars
User Docker
Cuando está ejecutando un trabajo de compilación de canalización de CI / CD, se sugiere usar estibador contenedores. Terraform proporciona contenedores Docker oficiales que se pueden utilizar. En caso de que esté cambiando el CI / CD servidor, puede pasar fácilmente la infraestructura dentro de un contenedor.

Antes de implementar la infraestructura en el entorno de producción, también puede probar la infraestructura en los contenedores Docker, que son muy fáciles de implementar. Al combinar Terraform y Docker, obtiene una infraestructura portátil, reutilizable y repetible.

# SRE Incident Response
## Being On-Call
## Before and Incident
## During an Incident
## After incident

